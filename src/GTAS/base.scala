/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package GTAS

import scala.collection.immutable.List
import scala.collection.immutable.HashMap

trait BaseType extends Serializable {
  implicit def id2val[S](a: Id[S]): S = a.value
  implicit def val2Id[V](a: V): Id[V] = new Id(a)
}

//Bag is a wrapper of Multi-List
class Bag[A](val a: List[List[A]]) extends BaseType {
  def this() = this(Nil)
  def apply(b: List[List[A]]) = new Bag[A](b)
  def get = a
  def :::(that: Bag[A]) = new Bag[A](a ::: that.get)
  def isEmpty: Boolean =  a == Nil 
  override def toString: String = "Bag{" + a.toString.drop(5).dropRight(1) + "}"
  def ==(that: Bag[A]) = { a == that.get }
  def toList = a
}

trait Countable[T] {
  def count: Int
}

trait Finite[T] extends Iterable[T] with Countable[T] {
}

trait Monoid[T] extends BaseType {
  //associative
  def combine(l: T, r: T): T
  def id: T
}

//has iterator
trait IterableMonoid[T] extends Monoid[T] with Iterable[T]

trait Morphism[M, +S] extends BaseType {
  def f(in: M): Option[S]
}

abstract class ListHomo[A, M] extends BaseType with Monoid[M] with Morphism[A, M] {
  //given a input list, the listhomomorphism can run on it
  def run1(x: List[A]): M = (id /: x) { (l, a) => combine(l, f(a).getOrElse(id)) }
  def run(x: List[A]): M = x.filter(f(_)!=None).foldLeft(id)((l, a) => combine(l, f(a).get)) 
  //def run(x: spark.RDD[A]):M =  x.map(f).reduce(combine)
}

trait MapReduceable[A, M, +R] extends ListHomo[A, M] {
  override def f(a: A): Option[M]
  override def combine(l: M, r: M): M
  def postProcess(a: M): R
  def compute(x: List[A]) = postProcess(run(x))
  def compute1(x: List[A]) = postProcess(run1(x))
  
//  def debug(x: List[A]) = run(x)

}


trait Semiring[S] extends BaseType {
  def times(l: S, r: S): S
  def id: S
  def plus(l: S, r: S): S
  def zero: S
}

/*
  * Semiring on Bag[A]
  */

class BagSemiring[A] extends Semiring[Bag[A]] {
  override def plus(l: Bag[A], r: Bag[A]) = l ::: r
  override def times(l: Bag[A], r: Bag[A]) =
    new Bag[A](for (x <- l.get; y <- r.get) yield (x ::: y))
  override def id: Bag[A] = new Bag[A](List[List[A]](Nil))
  override def zero: Bag[A] = new Bag[A](Nil)

  def bagUnion(l: Bag[A], r: Bag[A]): Bag[A] = plus(l, r)
  def crossConcat(l: Bag[A], r: Bag[A]): Bag[A] = times(l, r)
  def nilBag: Bag[A] = zero
  def bagOfNil: Bag[A] = id
}

/*
 * BagAggregator: only produces bags 
 */
class BagAggregator[A] extends Aggregator[A, Bag[A]] {
  val bagSemiring: BagSemiring[A] = new BagSemiring[A]
  override def times(l: Bag[A], r: Bag[A]) = bagSemiring.times(l, r)
  override def id: Bag[A] = bagSemiring.id
  override def plus(l: Bag[A], r: Bag[A]) = bagSemiring.plus(l, r)
  override def zero: Bag[A] = bagSemiring.zero
  override def f(a: A) = Some( new Bag[A](List(List(a))) )

}

trait Aggregator[A, S] extends Semiring[S] with Morphism[A, S] {
  def singleton(a: A): Option[S] = f(a)
  def bagUnion(l: S, r: S): S = plus(l, r)
  def nilBag: S = zero
  def crossConcat(l: S, r: S): S = times(l, r)
  def bagOfNil: S = id
}

class LiftAgg[I, T, Key](val test: Predicate[I, Key], val agg: Aggregator[I, T]) extends Aggregator[I, HashMap[Key, T]] {
  //make the lifted semiring
  override def plus(l: HashMap[Key, T], r: HashMap[Key, T]) = {
    var m = scala.collection.mutable.Map.empty[Key, T]
    l foreach (ll => m(ll._1) = ll._2)
    r foreach (rr => m(rr._1) = agg.plus(m.getOrElse(rr._1, agg.zero), rr._2))
    collection.immutable.HashMap[Key, T](m.toSeq: _*)
  }
  override def times(l: HashMap[Key, T], r: HashMap[Key, T]) = {
    var m = scala.collection.mutable.Map.empty[Key, T]
    l foreach (ll =>
      r foreach (rr => {
        val k = test.combine(ll._1, rr._1)
        val v = agg.times(ll._2, rr._2)
        m(k) = agg.plus(m.getOrElse(k, agg.zero), v)
      }))

    collection.immutable.HashMap[Key, T](m.toSeq: _*)

  }
  override def f(a: I) = Some(HashMap(test.f(a).get -> agg.f(a).get))
  override def id: HashMap[Key, T] = HashMap(test.id -> agg.id)
  override def zero: HashMap[Key, T] = HashMap.empty[Key, T]
}

/*
 * Predicate is almost-listhomomorphism
 * whose postProcess returns Boolean
 */
trait Predicate[M, T] extends MapReduceable[M, T, Boolean]

/*
 * FinitePredicate is Predicate on finite monoid
 */
trait FinitePredicate[M, T] extends Predicate[M, T] with Finite[T]

/*
 * ID is a type wrapper
 * copied form Emoto's impl.
 */
class Id[A](val value: A)extends Serializable {
  override def toString() = "Id(" + value + ")"
}



class Pair[+T](val l: T, val r: T) extends Serializable {
  def _1 = l
  def _2 = r
  override def toString = "("+_1 +"," + _2 +")"
}


class Pair2[T1,T2](val l:T1, val r:T2) extends Tuple2[T1,T2](l,r)

class T4[T](val a:T, val ss:T, val is:T, val ts:T) extends Tuple4[T,T,T,T](a,ss,is,ts)
