/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package GTAS

/*
 * Parallelizable GTA fusion: G+ T* + A = ListHomomorphism => MapReduce
 * 
 */
import scala.collection.immutable.HashMap


/*
 * ParallelGTA1[I] + ParallelGTA2
 */

trait GTA[I, AI] {
  def generate[GG <: GeneratorCreater[I, AI]](gg: GG) =  new GEN[I, AI](gg)
}

//trait GTALocal[I] extends ParallelGTA[I, Id]

class GEN[IN, AI](val gg: GeneratorCreater[IN, AI]) extends BaseType {
  def filter[Key](test: Predicate[AI, Key]): GEN2[IN, Key, AI] = {
    implicit val flag = false
    new GEN2[IN, Key, AI](gg, test)
  }
  def aggregate[S](agg: Aggregator[AI, S]): MapReduceable[IN, S, Any] =
    gg.makeGenerator(agg)
}
 
class GEN2[I, K, AI](val gg: GeneratorCreater[I, AI], val test: Predicate[AI, K]) extends BaseType {
  val flag = 0
  def compose2[K1, K2](test1: Predicate[AI, K1], test2: Predicate[AI, K2]): Predicate[AI, (K1, K2)] = {
    val newTest: Predicate[AI, (K1, K2)] = new Predicate[AI, (K1, K2)]() {
      override def f(a: AI): Some[(K1, K2)] = Some((test1.f(a).get, test2.f(a).get))
      override def combine(l: (K1, K2), r: (K1, K2)) = (test1.combine(l._1, r._1), test2.combine(l._2, r._2))
      override val id = (test1.id, test2.id)
      override def postProcess(x: (K1, K2)): Boolean = test1.postProcess(x._1) && test2.postProcess(x._2)
    }
    newTest
  }
  def filter[K2](test2: Predicate[AI, K2]): GEN2[I, (K, K2), AI] = {

    new GEN2(gg, compose2(test, test2))
  }

  def aggregate[S](agg: Aggregator[AI, S]): Generator[I, HashMap[K, S], AI] = {
    val gen = new Generator[I, S, AI](agg) {
      override def id[V](ag: Aggregator[AI, V]) = gg.makeGenerator(ag).id
      override def f[V](a: I, ag: Aggregator[AI, V]): V = {
        gg.makeGenerator(ag).f(a).get
      }
      override def combine[V](l: V, r: V, ag: Aggregator[AI, V]): V =
        gg.makeGenerator(ag).combine(l, r)
      override def postProcess(a: S): Any = gg.makeGenerator(agg).postProcess(a)
    }
    gen.filter[K](test)
  }

}

/*
 * Generator[IN, M]
 *   IN:  input list[IN]
 *   M:   out  a[M]
 *   postProcess: M => Any
 */
abstract class Generator[IN, M, AI](val agg: Aggregator[AI, M]) extends MapReduceable[IN, M, Any] {
  def f[V](a: IN, ag: Aggregator[AI, V]): V
  def combine[V](r: V, l: V, ag: Aggregator[AI, V]): V
  def id[V](ag: Aggregator[AI, V]): V

  override def combine(l: M, r: M): M = combine[M](l, r, agg)
  override def f(a: IN): Some[M] = Some(f[M](a, agg))
  override def postProcess(m: M): Any //= postProcess[M](agg,m)
  override def id = id[M](agg)

  def filter[Key](test: Predicate[AI, Key]): Generator[IN, HashMap[Key, M], AI] = {
    val base = this
    val lifted = new LiftAgg(test, base.agg)

    val ng = new Generator[IN, HashMap[Key, M], AI](lifted) {

      override def id[V](ag: Aggregator[AI, V]) = base.id(ag)
      override def f[V](a: IN, ag: Aggregator[AI, V]): V = base.f(a, ag)
      override def combine[V](r: V, l: V, ag: Aggregator[AI, V]): V = {
        base.combine(r, l, ag)
      }
      def postProcess(m: HashMap[Key, M]): Any = {

        val mm = m.foldLeft(base.agg.zero)((l, kv) => if (test.postProcess(kv._1)) base.agg.plus(l, kv._2) else l)
        base.postProcess(mm)
      }
    }
    ng
  }
}


