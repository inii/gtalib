/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

import mirrored.GTA
import GTAS.Id
import GTAS.T4
import mirrored.SegmentsGen
import GTAS.BagAggregator
import GTAS.Bag
import GTAS.MaxTotalValueInt
import GTAS.EvenLength
import GTAS.EvenIntLengthLimit

object testSegmentsGen extends GTA[Int, Int, T4]{

  def main(args: Array[String]): Unit = {
    //-1,2,3,-4
    val x = List[Int](-1,2,3,4,-4,6)
    val gta = generate(new SegmentsGen[Int]()) aggregate (new BagAggregator[Int]())
    println("test testSegmentsGen of x = " + gta.compute(x))
    //println("AllSelects of x length= " + gta.compute(x).asInstanceOf[Bag[Int]].get.length)
    
    //G+A
    val test2 = generate(new SegmentsGen[Int]()) aggregate (MaxTotalValueInt)
    println("MaxTotalValue = " + test2.compute(x))
    
        //G+T+A
    
    val test3 = generate(new SegmentsGen[Int]()) filter( new EvenLength[Int]) aggregate (MaxTotalValueInt)
    println("MaxTotalValue = " + test3.compute(x))
    
    val limit = new EvenIntLengthLimit(2)
    val test4 = generate(new SegmentsGen[Int]()) filter(limit) aggregate (MaxTotalValueInt)
    println("MaxTotalValue = " + test4.compute(x))
  }

}