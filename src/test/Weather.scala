/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

//object Weather extends Enumeration{
//  val Sunny, Rainny, Cloudy = Value
//}
//
//object Action extends Enumeration{
//  val Shopping, Reading, Sleeping = Value
//}

sealed abstract class Weather extends Serializable
case object Sunny extends Weather {
 override def toString = "Sunny"
}
case object Rainy extends Weather{
 override def toString = "Rainy"
}

case object Cloudy extends Weather{
 override def toString = "Cloudy"
}
case object NoState extends Weather{
 override def toString = "NoState"
}


sealed abstract class Action
case object Shopping extends Action
case object Reading extends Action
case object Sleeping extends Action
