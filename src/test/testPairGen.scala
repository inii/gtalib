/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

import mirrored.GTA
import GTAS.Id
import GTAS.Pair
import mirrored.PrefixsGen
import mirrored.SurfixsGen
import GTAS.BagAggregator
import GTAS.Bag
import GTAS.MaxTotalValueInt
import GTAS.EvenLength

/*
 * Sometimes to define aggregators and testers on Pair, Triple is a boring work
 * so the mirrored.GTA can be used
 */
object testPairGen extends GTA[Int, Int, Pair]{

  def main(args: Array[String]): Unit = {
    
    val x = List[Int](1,2,3,-4,5)
    val gta = generate(new PrefixsGen[Int]()) aggregate (new BagAggregator[Int]())
    println("inits of x = " + gta.compute(x))
    println("AllSelects of x length= " + gta.compute(x).asInstanceOf[Bag[Int]].get.length)
    
    //G+A
    
    val test2 = generate(new PrefixsGen[Int]()) aggregate (MaxTotalValueInt)
    println("MaxTotalValue = " + test2.compute(x))
    
        //G+T+A
    
    val test3 = generate(new PrefixsGen[Int]()) filter( new EvenLength[Int]) aggregate (MaxTotalValueInt)
    println("MaxTotalValue = " + test3.compute(x))
    
    
    val gta2 = generate(new SurfixsGen[Int]()) aggregate (new BagAggregator[Int]())
    println("tails of x = " + gta2.compute(x))
  }

}