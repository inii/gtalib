/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

import GTAS._
import DS.KnapsackItem
import scala.util.Random

object TestNewGF extends GTA[KnapsackItem, KnapsackItem] with App{
  
    def creatTestData(n: Int): List[KnapsackItem] = {
    //create n knapsack items
    val rand = new Random()
    val rst = for (x <- List.range(0, n)) yield new KnapsackItem(rand.nextInt(10), rand.nextInt(20))
    rst
  }
    
  val lst = creatTestData(10)
 // val rst = generate(new AllSelects2[Int]()) filter (new LengthLimit[Int](10)) aggregate (maxSum) compute (lst)
  val rst = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(100)) aggregate (maxTotalValue) compute (lst)
  println("result: " + rst)
  
  testEfficient.denamic(lst) // check the result
 
}