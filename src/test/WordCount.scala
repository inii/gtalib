/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

import scala.List

object Length{
 
  def f(in: String) = 1
  def combine (a:Int, b:Int) = a+b
  def id = 0
}

object WordCount {

  //for creation of multi-lists
  def init(text:List[String], f: String => Int) = {
     for (str <- text if (! str.isEmpty()) ) yield (str, f(str)) 
  }
  
  def groupByKey(lists : List[(String,Int)])= {
      lists.groupBy( par => par._1 )
  }
  
  def main(args: Array[String]): Unit = {
    
    val inputText = List[String]("we", "learn", "Chinese" , "Japanese" ,"and" ,"learn" ,"English", "and" , "also")
    
    // framework does this, using user's f
    val markAsDifferentLists = init(inputText, Length.f) 
    //framework does this
    val intermediate = groupByKey(markAsDifferentLists) 
    
    //for each list do reduce (parameterized by user' combine)
    val result = for(list <- intermediate.values) yield list.reduce( (a,b) => (a._1, Length.combine(a._2, b._2) ) ) 
        //reduce( (a,b) => (a._1, a._2+b._2))
    for( rst <- result ) print {rst._1 + ":" + rst._2 + " " }
  }

}