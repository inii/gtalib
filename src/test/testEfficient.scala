/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package test

import GTAS._
import DS.KnapsackItem
import Examples.KnapsackLocal
import Examples.KnapsackSpark

object testEfficient extends GTA[KnapsackItem, KnapsackItem] {

  def gta(lst: List[KnapsackItem]) = {
    val rst = generate(new AllSelects[KnapsackItem]()) filter (new WeightLimit(100)) aggregate (maxTotalValue) compute (lst)

    println("result: " + rst)
  }

  def time(name: String, f: => Unit) = {
    val s = System.currentTimeMillis
    f
    val t = (System.currentTimeMillis - s)

    println("+++++ Function " + name + " takes " + t + "millis")
    println("+++++")
  }

  def naive(x: List[KnapsackItem]) = {
    val candidates = KnapsackLocal.generate(x, new AllSelects1[KnapsackItem]()) aggregate (new BagAggregator[KnapsackItem]())
    val rst = candidates.toList.filter(e => e.foldLeft(0)((n, itm) => n + itm.weight) <= 100).reduce((e1, e2) =>
      if ((e1.foldLeft(0)((n, it) => n + it.value)) >= (e2./:(0)((n, it) => n + it.value))) e1 else e2)
    println("result: " + rst.foldLeft(0)((n, it) => n + it.value))
  }

  def compareGTAandNaive {
    println("start comparison")
    val testcases = List.range(8, 32, 4)
    def compare(n: Int) = {
      println("comparison: " + n)
      val x = KnapsackSpark.creatTestData(n)
      time("liu-GTA: " + n, gta(x))
      time("naive g + T + A", naive(x))
    }
    testcases.foreach(compare)
    println("over +++++")
  }

  /*
   * test GTA in local with different length of input 
   * to see the algorithm is in linear
   */
  def compareLength {
    println("start ====>")
    val testcases = List.range(1000, 8000, 1000)
    def compare(n: Int) = {
      println("comparison: " + n)
      val x = KnapsackSpark.creatTestData(n)
      //KnapsackSpark.time("naive g + T + A", naive(x))
      time("liu-GTA: " + n, gta(x))
    }

    testcases.foreach(compare)
    println("over +++++")
  }

  def compareGTAandDP {
    println("start comparison")
    val testcases = List.range(1000, 6000, 1000)
    def compare(n: Int) = {
      println("comparison: " + n)
      val x = KnapsackSpark.creatTestData(n)
      time("DP: " + n, denamic(x))
      time("liu-GTA: " + n, gta(x))

    }
    testcases.foreach(compare)
    println("over +++++")
  }

  def denamic(loi: List[KnapsackItem]) = {
    //===== dynamic programming ==========================================================
    // slower than GTA
    val W = 100
    val N = loi.size

    val m = Array.ofDim[Int](N + 1, W + 1)
    val plm = (List((for { w <- 0 to W } yield Set[KnapsackItem]()).toArray) ++ (
      for {
        n <- 0 to N - 1
        colN = (for { w <- 0 to W } yield Set[KnapsackItem](loi(n))).toArray
      } yield colN)).toArray

    1 to N foreach { n =>
      0 to W foreach { w =>
        def in = loi(n - 1)
        def wn = loi(n - 1).weight
        def vn = loi(n - 1).value
        if (w < wn) {
          m(n)(w) = m(n - 1)(w)
          plm(n)(w) = plm(n - 1)(w)
        } else {
          if (m(n - 1)(w) >= m(n - 1)(w - wn) + vn) {
            m(n)(w) = m(n - 1)(w)
            plm(n)(w) = plm(n - 1)(w)
          } else {
            m(n)(w) = m(n - 1)(w - wn) + vn
            plm(n)(w) = plm(n - 1)(w - wn) + in
          }
        }
      }
    }

    val tv = m(N)(W)

    // println{val h = "packing list of items (dynamic programming):"; h+"\n"+"="*h.size}
    //plm(N)(W).foreach{p=>print("  " +": weight="+p.weight+" value="+p.value+"\n")}
    //println("\n"+"  resulting items: "+plm(N)(W).size+" of "+loi.size) 
    // println("  total weight: "+(0/:plm(N)(W).map{item=>item.weight})(_+_)+", total value: "+m(N)(W))
    println("result DP: " + tv)
  }

  def main(args: Array[String]) {

//    val allPossibles = new AllSelects[KnapsackItem]
//    val withLimit_100 = new WeightLimit(100)
//
//    val gta = generate(allPossibles) filter withLimit_100 aggregate maxTotalValue

     compareGTAandNaive

    //compareLength
    //compareGTAandDP

  }

}