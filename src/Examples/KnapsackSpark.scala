/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package Examples

import GTAS._
import scala.Int
import scala.util.Random
import scala.collection.immutable.HashMap
import DS.KnapsackItem
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD

object KnapsackSpark extends GTA[KnapsackItem, KnapsackItem] {

  def creatTestData(n: Int): List[KnapsackItem] = {
    //create n knapsack items
    val rand = new Random()
    val rst = for (x <- List.range(0, n)) yield new KnapsackItem(rand.nextInt(10), rand.nextInt(20))
    rst
  }

   // create test data in memory
  def createTestDataInMem(contex: SparkContext, slices: Int, length: Int): RDD[KnapsackItem] = {
    val seed = List.range(0, slices * 2)
    val rdd = contex.parallelize(seed, slices).flatMap(e => creatTestData(length / (slices * 2)): List[KnapsackItem])
    //println("input length = " + rdd.count + " slices=" + slices)
    rdd
  }
  
  //G+T+A
  def testSparkKnapsack(contex: SparkContext, x: RDD[DS.KnapsackItem], tw:Int) = {
    
    
    val allSellects = new AllSelects[KnapsackItem]
    val withLimit = new WeightLimit(tw)
    val gta = generate(allSellects)   .
               filter (withLimit) .  
                      aggregate (maxTotalValue) 
    val rst = gta.postProcess(x.map(gta.f(_).get).reduce(gta.combine(_, _)))
    
    val weightS:String = tw.toString();
    
    ("+++++++++++++++ knapsack WeightLimit="+ weightS + " = " + rst)
  }
  
  def testDifferentW(sp: SparkContext, x: RDD[DS.KnapsackItem]) = {
    val  str1 = testSparkKnapsack(sp, x, 50)
    //println("+++++++++++++++ knapsack with WeightLimit  = 100")
    
    val  str2 = testSparkKnapsack(sp, x, 100)
    //println("+++++++++++++++ knapsack with WeightLimit  = 1000")
    
    val  str3 = testSparkKnapsack(sp, x, 200)
    //println("+++++++++++++++ knapsack with WeightLimit  = 10000")
    
    println("=======")
    println("===1===" + str1)
    println("===2===" + str2)
    println("===3==="+ str3)
  }

  //G+A
  def testSparkGwithA(contex: SparkContext, x: RDD[DS.KnapsackItem]) = {
 
    //run mapreduce
    val generator = generate(new AllSelects[KnapsackItem]()) aggregate (maxTotalValue)
    val inter = x.map(generator.f(_).get).reduce(generator.combine(_, _))
    val rst = generator.postProcess(inter)
    println("+++++++++++++++ Knapsack AllSelects + MaxTotalValue = " + rst)
    println("+++++++++++++++JOB2 ACCOMPLISHED++++++++++++++")
  }

  //SUM ALL
  def sumall(contex: SparkContext, x: RDD[DS.KnapsackItem]) = {

    def f(l:KnapsackItem, r:KnapsackItem) = new KnapsackItem( l.value + r.value, 0)
    val rst = x.reduce(f(_,_)).value
    println("+++++++++++++++ SUM ALL VALUE = " + rst)
    println("+++++++++++++++JOB3 ACCOMPLISHED++++++++++++++")
  }

  def main(args: Array[String]) {

    val spark = new SparkContext(args(0), "Spark_Knapsack", System.getenv("SPARK_HOME"), List(System.getenv("MY_APPS") + "/gtas.jar",System.getenv("MY_APPS") + "/ds.jar"))
    if (args.length == 0) {
      System.err.println("Usage: KnapsackSpark <host> [<slices>] [<length of input-list>] [<weightLimit>]")
      System.exit(1)
    }

    
    val slices = if (args.length > 1) args(1).toInt else 4
    val length = if (args.length > 2) args(2).toInt else 10000
    //val tw = if (args.length > 3) args(3).toInt else 100
    
    val x = createTestDataInMem(spark, slices, length).cache
    println("+++++ generate all data => size=" + x.count + " +++++")

    //testSparkGwithA(spark, x)
    
    //println("+++++ pass1 x => size=" + x.count + " +++++")
    //testSparkKnapsack(spark, x, tw)
    //println("+++++pass2 x => size=" + x.count + " +++++")
    //sumall(spark, x)

    // compareGTAandNaive

    // testSerialization(1000)
    
    testDifferentW(spark,x)

    System.exit(0)

  }
}
