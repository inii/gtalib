/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package Examples

import GTAS._
import scala.util.Random
import DS.KnapsackItem
import GTAS.EMOTO.GTAEM


object KnapsackLocal extends GTAEM {

  def test(lst: List[KnapsackItem]) {
    val itemA = new KnapsackItem(3, 2)
    val itemB = new KnapsackItem(2, 2)
    val x = List(new KnapsackItem(6, 1), itemA, itemB) ::: lst

    println("input = " + x + " length=" + x.length)
    

    
    val test1 = generate(x, new AllSelects1[KnapsackItem]()) aggregate (new BagAggregator[KnapsackItem]())
    println("AllSelects of x = " + test1)

    val test2 = generate(x, new AllSelects1[KnapsackItem]()) aggregate (maxTotalValue)
    println("MaxTotalValue = " + test2)

    val test3 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(4)) aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 4 = " + test3)

    val getSolution = new SelectiveAggregator(maxTotalValue, new BagAggregator[KnapsackItem])
    val rst = generate(x, new AllSelects1[KnapsackItem]()). filter (new WeightLimit(4)) aggregate (getSolution)
    println("The solution is " + rst)

    // x = x ::: x ::: x
    //println("new input = " + x)
    //generate + filter + aggregate
    val test4 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 6 = " + test4)
    //generate + filter + aggregate
    val test5 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) aggregate (getSolution)
    println("The selection is " + test5)
    //multi filter
    val test6 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) filter (EvenNumberKnapsackItems) aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 6 and even # of items = " + test6)
    //multi filter
    val test7 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) filter (EvenNumberKnapsackItems) aggregate (getSolution)
    println("The selection is " + test7)

    //multi filter
    val test8 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) filter (new A_needs_B(itemA, itemB)) aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 6 and A=>B = " + test8)

    //multi filter
    val test9 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) filter (new A_needs_B(itemA, itemB)) aggregate (getSolution)
    println("The selection is " + test9)

    //multi filter
    val test10 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) filter (Ascending) aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 6 and ascending = " + test10)

    //multi filter
    val test11 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) filter (Ascending) aggregate (getSolution)
    println("The selection is " + test11)

    val test12 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) filter (Ascending) filter (new A_needs_B(itemA, itemB)) aggregate (maxTotalValue)
    println("MaxTotalValue with WeightLimit 6 and" + itemA + "=>" + itemB + " and Ascending= " + test12)
    val test13 = generate(x, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(6)) filter (Ascending) filter (new A_needs_B(itemA, itemB)) aggregate (getSolution)
    println("The selection is " + test13)
  }

  //run the test
  //test(Nil)
  def creatTestData(n: Int): List[KnapsackItem] = {
    //create n knapsack items
    val rand = new Random()
    val rst = for (x <- List.range(0, n)) yield new KnapsackItem(rand.nextInt(10), rand.nextInt(20))
    rst
  }

  def time(name: String, f: => Unit) = {
    val s = System.currentTimeMillis
    f
    val t = (System.currentTimeMillis - s)

    println("Function " + name + " takes " + t + "millis")
  }
  
  def naive( x: List[KnapsackItem] ) = {
    val candidates = generate(x, new AllSelects1[KnapsackItem]()) aggregate (new BagAggregator[KnapsackItem]())
    val rst = candidates.toList.filter(e => e.foldLeft(0)( (n, itm) => n+ itm.weight ) <= 100 ).reduce((e1,e2) => 
           if ( (e1.foldLeft(0)((n,it) => n+ it.value)) >= (e2./:(0)((n,it) => n+ it.value) ) ) e1 else e2 )
    println("result: " + rst.foldLeft(0)((n,it) => n+ it.value))       
  }

  def compareGTAandNaive {

    println("start comparison")
    val x = creatTestData(20)
    def emoto(lst : List[KnapsackItem]) = { 
     val rst = generate(lst, new AllSelects1[KnapsackItem]()) filter (new WeightLimit(100)) aggregate (maxTotalValue)
      
     println("result: " + rst)  
    }
    
    time( "naive g + T + A", naive(x))
    
    time ("emoto", emoto(x))

  }

  def main(args: Array[String]) {

    compareGTAandNaive
  }
}