/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package Examples

import test._
import GTAS.Id
import GTAS.Probabilities
import GTAS.Aggregator
import GTAS.MapReduceable
import GTAS.Predicate
import GTAS.MaxProdAggregator
import scala.util.Random
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object ViterbiSpark extends mirrored.GTA[Action, Tuple2[Action, (Weather, Weather)],Id]{

  val states = Set(Sunny, Rainy, Cloudy)

  //type Transition = List[(Weather, Weather)]
  type MarkedTs = Tuple2[Action, (Weather, Weather)]
  type Mark =  Tuple2[Weather, Weather]

  val probs = new Probabilities[Action,Weather]{
    override  def envtProb(evn:Action, st:Weather):Double = st match{
      case  Sunny => evn match {
            case  Shopping => 0.8
            case  Reading  => 0.4
            case  Sleeping => 0.1
            case _ => throw new IllegalArgumentException("no possible")
      }
     case Rainy => evn match {
            case  Shopping => 0.1
            case  Reading  => 0.4
            case  Sleeping => 0.8
            case _ => throw new IllegalArgumentException("no possible")
      }
     case Cloudy => evn match {
            case  Shopping => 0.4
            case  Reading  => 0.8
            case  Sleeping => 0.2
            case _ => throw new IllegalArgumentException("no possible")
      }
     case _ => throw new IllegalArgumentException("no such things")
    }   
            
    override  def transProb(st1:Weather, st2:Weather):Double = st1 match {
      case Sunny => st2 match{
        case Sunny => 0.6
        case Rainy => 0.4
        case Cloudy => 0.4
        case _ => throw new IllegalArgumentException("no possible")
      }
       case Rainy => st2 match{
        case Sunny => 0.4
        case Rainy => 0.2
        case Cloudy => 0.7
        case _ => throw new IllegalArgumentException("no possible")
      }
     case Cloudy => st2 match{
        case Sunny => 0.4
        case Rainy => 0.7
        case Cloudy => 0.5
        case _ => throw new IllegalArgumentException("no possible")
      }
        case _ => throw new IllegalArgumentException("no such things")
    }

  }
  //tail recursion
  def chechTrans(a: Mark): Boolean = {
  if (a  == (NoState, NoState))
      false
    else
      true
  }

  object assTransGen extends mirrored.GeneratorCreater[Action, Tuple2[Action, (Weather, Weather)], Id] {
  val marks = for(x <- states ; y <-states) yield (x,y) // List(x,y) ??
  
  override def makeGenerator[S](s: Aggregator[Tuple2[Action, (Weather, Weather)], S]): MapReduceable[Action, Id[S], S] =
    new MapReduceable[Action, Id[S], S] {
      def f(i: Action): Some[Id[S]] = Some(marks.foldLeft(s.zero)((z, mk) => s.plus(z, s.f(Tuple2(i, mk)).get)))
      def combine(l: Id[S], r: Id[S]): Id[S] = s.times(l, r)
      val id: Id[S] = s.id
      def postProcess(a: Id[S]): S = a
    }
}
  
  object viterbiTest extends Predicate[MarkedTs, Mark] {
    /*
     * this looks not a list homomorphism
     */
    def postProcess(a: Mark) = chechTrans(a)
    def combine(l: Mark, r: Mark) = {
      if (l == (null, null)) r
      else {
        (l._2.toString() == r._1.toString()) match {
          case true => (l._1, r._2)
          case _ => Tuple2[Weather, Weather](NoState, NoState)
        }
      }
    }
    def f(a: MarkedTs): Option[Mark] = Some( a._2 )
    val id: Mark = Tuple2[Weather,Weather](null,null)
  }

  object viterbiAgg extends MaxProdAggregator[MarkedTs] {
   override def f(mts :MarkedTs):Some[Double] ={
     val event = mts._1
     val trans = mts._2
     val prob = probs.transProb(trans._1, trans._2) * probs.envtProb(event,trans._2)
     Some[Double](prob) 
   }
  }


  def creatTestData(n: Int): List[Action] = {
    //create n knapsack items
    val rand = new Random()
    val x = List.range(0, n)
    val rst = x.map(_ =>
      rand.nextInt(10) % 3 match {
        case 0 => Shopping
        case 1 => Reading
        case 2 => Sleeping
      })
    rst
  }
  
  def createTestDataInMem(contex: SparkContext, slices: Int, length: Int): RDD[Action] = {
    val seed = List.range(0, slices * 2)
    val rdd = contex.parallelize(seed, slices).flatMap(e => creatTestData(length / (slices * 2)): List[Action])
    //println("input length = " + rdd.count + " slices=" + slices)
    rdd
  }
  
  def main(args: Array[String]) {

    val spark = new SparkContext(args(0), "Spark_Viterbi", System.getenv("SPARK_HOME"), List(System.getenv("MY_APPS") + "/gtas.jar"))
    if (args.length == 0) {
      System.err.println("Usage: ViterbiSpark <host> [<slices>] [<length of input-list>]")
      System.exit(1)
    }

    val length = if (args.length > 2) args(2).toInt else 10000
    val slices = if (args.length > 1) args(1).toInt else 4
    val x = createTestDataInMem(spark, slices, length)
    println("+++++ generate all data => size=" + x.count + " +++++")
    val gta = generate(assTransGen) filter (viterbiTest) aggregate(viterbiAgg)
    val rst = gta.postProcess(x.map(gta.f(_).get).reduce(gta.combine(_, _)))
    println("+++++ result = " + rst)
    System.exit(0)

  }
}