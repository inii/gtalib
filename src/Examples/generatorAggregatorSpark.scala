/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package Examples

import DS.KnapsackItem
import GTAS._
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext

object generatorAggregatorSpark extends GTA[KnapsackItem, KnapsackItem] with App {

  def knapsack(contex: SparkContext, input: RDD[DS.KnapsackItem]) = {
    val gta = generate(new AllSelects[KnapsackItem]()) aggregate (maxTotalValue)
    val inter = input.map(gta.f(_).get).reduce(gta.combine(_, _))
    val rst = gta.postProcess(inter)
    println("knapsack AllSelects + MaxTotalValue = " + rst)
  }
}