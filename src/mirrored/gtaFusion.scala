/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package mirrored

import GTAS.BaseType
import GTAS.Aggregator
import GTAS.MapReduceable
import GTAS.Predicate
import GTAS.FinitePredicate
import scala.collection.immutable.HashMap
import GTAS.LiftAgg

trait GTA[I,AI,P[_]] {
  def generate[GG <: GeneratorCreater[I,AI, P]](gg: GG) =  new GEN3F[I,AI, P](gg)
}

trait GeneratorCreater[I, AI, P[_]] extends BaseType {
  def makeGenerator[S](s: Aggregator[AI, S]): MapReduceable[I, P[S], S]
}

class GEN3F[IN, AI, P[_]](val gg: GeneratorCreater[IN, AI,P]) extends BaseType {
  def filter[Key](test: Predicate[AI, Key]) = {
    new GEN3F2[IN, AI, Key, P](gg, test)
  }

  def aggregate[S](agg: Aggregator[AI, S]): MapReduceable[IN, P[S], Any] =
    gg.makeGenerator(agg)
}

class GEN3F2[I,AI, K, P[_]](val gg: GeneratorCreater[I, AI,P], val test: Predicate[AI, K]) extends BaseType {

  def compose2[T,K1, K2](test1: Predicate[T, K1], test2: Predicate[T, K2]): Predicate[T, (K1, K2)] = {
    val newTest: Predicate[T, (K1, K2)] = new Predicate[T, (K1, K2)]() {
      override def f(a: T): Some[(K1, K2)] = Some((test1.f(a).get, test2.f(a).get))
      override def combine(l: (K1, K2), r: (K1, K2)) = (test1.combine(l._1, r._1), test2.combine(l._2, r._2))
      override val id = (test1.id, test2.id)
      override def postProcess(x: (K1, K2)): Boolean = test1.postProcess(x._1) && test2.postProcess(x._2)
    }
    newTest
  }

  //FinitePredicate not finished
  def creatFinitePredicate[T,K](finit: FinitePredicate[T, K], infinit: Predicate[T, K]) = {

  }

  def compose22[I,K1, K2](test1: Predicate[I, K1], test2: Predicate[I, K2]): FinitePredicate[I, (K1, K2)] = (test1, test2) match {
    // 2 finite monoids
    case (test1: FinitePredicate[I, K1], test2: FinitePredicate[I, K2]) =>
      new FinitePredicate[I, (K1, K2)] {
        override def f(a: I): Some[(K1, K2)] = Some( (test1.f(a).get, test2.f(a).get) )
        override def combine(l: (K1, K2), r: (K1, K2)) = (test1.combine(l._1, r._1), test2.combine(l._2, r._2))
        override val id = (test1.id, test2.id)
        override def postProcess(x: (K1, K2)): Boolean = test1.postProcess(x._1) && test2.postProcess(x._2)
        override def iterator = new Iterator[(K1, K2)] {
          private val itr1 = test1.iterator
          private val itr2 = test2.iterator
          val k1 = itr1.next
          val k2 = itr2.next
          def next = {
            if (itr2.hasNext)
              (k1, itr2.next)
            else
              (itr1.next, k2)
          }
          def hasNext = itr1.hasNext || itr2.hasNext
        }
        override def count: Int = test1.count * test2.count
      }

    // 1 finite monoid + 1 monoid ?

    // need at least one finite monoid
    case _ => throw (new Exception("GTA:need at least one finite monoid"))
  }

  def filter[K2](test2: Predicate[AI, K2]): GEN3F2[I, AI,(K, K2), P] = {
    new GEN3F2(gg, compose2(test, test2))
  }

  def aggregate[S](agg: Aggregator[AI, S]): MapReduceable[I, P[HashMap[K, S]], Any] = {
    val gen = new Generator[I,AI, S, P](gg, agg) {
      override def id[V](ag: Aggregator[AI, V]) = gg.makeGenerator(ag).id
      override def f[V](a: I, ag: Aggregator[AI, V]): P[V] = {
        gg.makeGenerator(ag).f(a).get
      }
      override def combine[V](l: P[V], r: P[V], ag: Aggregator[AI, V]): P[V] =
        gg.makeGenerator(ag).combine(l, r)
      override def postProcess(a: P[S]): S = gg.makeGenerator(agg).postProcess(a)
    }
    gen.embed[K](test)

  }

}


abstract class Generator[IN, AI ,M, P[_]](val gg: GeneratorCreater[IN,AI, P], val agg: Aggregator[AI, M]) extends MapReduceable[IN, P[M], Any] {
  def f[V](a: IN, ag: Aggregator[AI, V]): P[V]
  def combine[V](r: P[V], l: P[V], ag: Aggregator[AI, V]): P[V]
  def id[V](ag: Aggregator[AI, V]): P[V]

  override def combine(l: P[M], r: P[M]): P[M] = combine[M](l, r, agg)
  override def f(a: IN): Some[P[M]] = Some(f[M](a, agg))
  override def postProcess(m: P[M]): Any //= postProcess[M](agg,m)
  override def id = id[M](agg)

  def embed[Key](test: Predicate[AI, Key]): Generator[IN, AI,HashMap[Key, M], P] = {
    val base = this
    val lifted = new LiftAgg(test, base.agg)
    val ng = new Generator[IN,AI, HashMap[Key, M], P](gg, lifted) {

      val tt = gg.makeGenerator(agg)
      override def id[V](ag: Aggregator[AI, V]) = base.gg.makeGenerator(ag).id
      override def f[V](a: IN, ag: Aggregator[AI, V]): P[V] = base.f(a, ag)
      override def combine[V](r: P[V], l: P[V], ag: Aggregator[AI, V]): P[V] = {
        base.combine(r, l, ag)
      }
      override def postProcess(m: P[HashMap[Key, M]]): M = {
        val mm = tt.postProcess(m)
        val rst = mm.foldLeft(base.agg.zero)((l, kv) => if (test.postProcess(kv._1)) base.agg.plus(l, kv._2) else l)
        rst
      }

    }

    ng
  }
}

