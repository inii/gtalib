/*
 * Copyright (C) 2014 Yu Liu <yuliu@nii.ac.jp>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package mirrored

import GTAS.BaseType
import GTAS.Aggregator
import GTAS.MapReduceable
import GTAS.Predicate
import GTAS.FinitePredicate
import scala.collection.immutable.HashMap
import GTAS.LiftAgg
import GTAS.Id
import GTAS.Pair
import GTAS.T4

/*
 * this class allows users define a ParalAccumlator[A,B,C] without the limitation of B==C
 * but C is 'Any' here, return type of postProcess is implicitly given
 * and 'B' is bounded by 'S', it cannot be any structure over 'S',i.g., '(S,S)' 
 */
/**
 * This class is deprecated
 * @deprecated
 */
class AllSelects[K] extends GeneratorCreater[K,K, Id] {

  def makeGenerator[S](s: Aggregator[K, S]) = new MapReduceable[K, Id[S], S] {
    override def f(i: K): Some[Id[S]] = Some(s.bagUnion(s.singleton(i).get, s.bagOfNil) )
    override def combine(l: Id[S], r: Id[S]): Id[S] = s.crossConcat(l, r)
    override def postProcess(a: Id[S]): S = a
    override val id: Id[S] = s.bagOfNil
  }
}


/**
 * This class is deprecated
 * @deprecated
 */
class Prefixs[K, P](s: Aggregator[K, P]) extends MapReduceable[K, (P, P), P] {
  override def f(i: K): Some[(P, P)] = Some((s.plus(s.id, s.f(i).get), s.plus(s.zero, s.f(i).get)))
  override def combine(l: (P, P), r: (P, P)): (P, P) = (s.plus(l._1, s.times(l._2, r._1)), s.times(l._2, r._2))
  override def postProcess(a: (P, P)): P = a._1
  override val id: (P, P) = (s.zero, s.id)
}


class PrefixsGen[K] extends GeneratorCreater[K,K, Pair] {
  def makeGenerator[S](s: Aggregator[K, S]) = new MapReduceable[K, Pair[S], S] {
    override def f(i: K) =
      Some(new Pair[S](s.plus(s.id, s.f(i).get), s.plus(s.zero, s.f(i).get)))
    override def combine(l: Pair[S], r: Pair[S]): Pair[S] =
      new Pair[S](s.plus(l.l, s.times(l.r, r.l)), s.times(l.r, r.r)) 
    override val id: Pair[S] = new Pair[S](s.zero, s.id)
    override def postProcess(a: Pair[S]): S = a.l
  }
}

class SurfixsGen[K] extends GeneratorCreater[K,K, Pair] {
  def makeGenerator[S](s: Aggregator[K, S]) = new MapReduceable[K, Pair[S], S] {
    override def f(i: K) =
      Some(new Pair[S](s.plus(s.id, s.f(i).get), s.plus(s.zero, s.f(i).get)))
    override def combine(l: Pair[S], r: Pair[S]): Pair[S] =
      new Pair[S](s.plus(r.l, s.times(l.l, r.r)), s.times(l.r, r.r)) 
    override val id: Pair[S] = new Pair[S](s.zero, s.zero)
    override def postProcess(a: Pair[S]): S = a.l
  }
}

class SegmentsGen[I] extends GeneratorCreater[I, I, T4] {
  def makeGenerator[S](s: Aggregator[I, S]) = new MapReduceable[I, T4[S], S] {
    override def f(i: I) = Some(new T4(s.f(i).get, s.plus(s.id, s.f(i).get), s.plus(s.id, s.f(i).get), s.plus(s.zero, s.f(i).get)))
    override def combine(l: T4[S], r: T4[S]): T4[S] = {
      val inits = s.plus(l._3, s.times(l._4, r._3))
      val all = s.times(l._4, r._4)
      val ss = s.plus(s.plus(l._1, r._1), s.times(l._2, r._3)) 
      val tails = s.plus(r._2, s.times(l._2, r._4))
      new T4( ss , tails, inits, all)
    }
    override val id: T4[S] = new T4(s.zero, s.zero, s.zero, s.id)
    override def postProcess(a: T4[S]): S = a._1
  }
}